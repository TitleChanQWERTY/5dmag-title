using TMPro;
using UnityEngine;
using UnityEngine.UI;

public sealed class HealthController : MonoBehaviour
{
    [SerializeField] private float maxHealthPoint;
    
    [Header("Adiction")]
    [SerializeField] private Slider healthSlider;
    [SerializeField] private TextMeshProUGUI healthText;

    private float _currentHealth;

    private float CurrentHealth => _currentHealth;

    private void Awake()
    {
        _currentHealth = maxHealthPoint;
        if (healthSlider != null) healthSlider.maxValue = maxHealthPoint;
        UpdateHPUI();
    }

    private void Damage(float _damage)
    {
        _currentHealth -= _damage;
        UpdateHPUI();

        if (_currentHealth <= 0) Death();
    }

    private void UpdateHPUI()
    {
        if (healthSlider != null) healthSlider.value = _currentHealth;
        if (healthText != null) healthText.SetText($"HP: {_currentHealth}");
    }

    private void Death()
    {
        
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        var _enemy = collision.gameObject.GetComponentInParent<ZombieControll>();
        if (!_enemy) return;
        Damage(_enemy.Damage);
    }
}
