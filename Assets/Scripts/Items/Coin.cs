using UnityEngine;

public sealed class Coin : MonoBehaviour
{
    [SerializeField] private int coinValue;

    private Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _animator.SetBool("idle", true);
        _animator.SetBool("colected", false);
    }

    private void OnTriggerEnter(Collider other)
    {
        var _playerStats = other.GetComponent<PlayerStatsCtrl>();
        if (!_playerStats) return;
        _animator.SetBool("idle", false);
         _animator.SetBool("colected", true);
        _playerStats.AddCoins(coinValue);
    }

    public void DestroyCoin()
    {
        Destroy(gameObject);
    }

}
