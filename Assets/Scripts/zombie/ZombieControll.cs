using UnityEngine;
using UnityEngine.AI;

public sealed class ZombieControll : MonoBehaviour
{
    // Akiro! Fuck your zombie!

    [Header("main Parameters")]
    [SerializeField] private float zombieMaxHealth;
    [SerializeField] private float zombieCurrentHealth;

    [SerializeField] private float damage;

    [SerializeField] private float _chaseDistance;
    private float _distanceToPlayer;

    [SerializeField] private float zombieDamage;

    private bool zombieIsDead;
    private bool playerIsDetected;

    private GameObject _player;
    private Animator _animator;
    private NavMeshAgent _agent;

    [Header("References")]
    [SerializeField] private GameObject rightHandAtack;
    [SerializeField] private GameObject leftHandAtack;

    [SerializeField] private GameObject ragdollZombie;

    public float Damage => damage;
    public float ZombieDamage => zombieDamage;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _agent = GetComponent<NavMeshAgent>();
        playerIsDetected = false;
        zombieIsDead = false;
        zombieCurrentHealth = zombieMaxHealth;
        DisableDamageColliders();
    }

    private void Update()
    {
        if (zombieIsDead) return;

        if (_player != null)
        {
            _distanceToPlayer = Vector3.Distance(gameObject.transform.position, _player.transform.position);

            if (_distanceToPlayer < _chaseDistance)
            {
                    _animator.SetBool("idle", false);
                    _animator.SetBool("run", false);
                    _animator.SetBool("scream", true);
                    _animator.SetBool("atack1", false);
                    _animator.SetBool("kicking", false);
                    _animator.SetBool("punch", false);
                    _animator.SetBool("death", false);
                    _animator.SetBool("hit", false);

                if (playerIsDetected) FightLogic(); 
            }
            else if (_distanceToPlayer > _chaseDistance) IdelingLogic();
        }
        else _player = GameObject.FindGameObjectWithTag("Player");
    }


    private void IdelingLogic()
    {
        _animator.SetBool("idle", true);
        _animator.SetBool("run", false);
        _animator.SetBool("scream", false);
        _animator.SetBool("atack1", false);
        _animator.SetBool("kicking", false);
        _animator.SetBool("punch", false);
        _animator.SetBool("death", false);
        _animator.SetBool("hit", false);
    }
    private void FightLogic()
    {
        if (_distanceToPlayer <= _agent.stoppingDistance)
        {
            _animator.SetBool("idle", false);
            _animator.SetBool("run", false);
            _animator.SetBool("scream", false);
            _animator.SetBool("atack1", true);
            _animator.SetBool("kicking", false);
            _animator.SetBool("punch", false);
            _animator.SetBool("death", false);
            _animator.SetBool("hit", false);
        }
        else if (_distanceToPlayer > _agent.stoppingDistance)
        {
            _agent.SetDestination(_player.transform.position);

            _animator.SetBool("idle", false);
            _animator.SetBool("run", true); 
            _animator.SetBool("scream", false);
            _animator.SetBool("atack1", false);
            _animator.SetBool("kicking", false);
            _animator.SetBool("punch", false);
            _animator.SetBool("death", false);
            _animator.SetBool("hit", false);
        }
    }
    private void DeathLogic()
    {
        Instantiate(ragdollZombie, transform.position, transform.rotation);
        Destroy(gameObject);
    }

    private void ClaimDamage(float damageToClaim)
    {
        _animator.SetBool("hit", true); // idk, this anim do not want to play itself. 
        _animator.SetBool("idle", false);
        _animator.SetBool("run", false);
        _animator.SetBool("scream", false);
        _animator.SetBool("atack1", false);
        _animator.SetBool("kicking", false);
        _animator.SetBool("punch", false);
        _animator.SetBool("death", false);
        zombieCurrentHealth -= damageToClaim;
        if (zombieCurrentHealth <= 0) DeathLogic();
    }


    private void OnCollisionEnter(Collision collision)
    {
        var _sword = collision.gameObject.GetComponent<Sword>();
        if (!_sword) return;
        ClaimDamage(_sword.WeaponDamage);
    }
    public void ZombieScreamed()
    {
        playerIsDetected = true;
    }

    public void AnableDamageColliders()
    {
        rightHandAtack.SetActive(true);
        leftHandAtack.SetActive(true);
    }
    public void DisableDamageColliders()
    {
        rightHandAtack.SetActive(false);
        leftHandAtack.SetActive(false);
    }
}
