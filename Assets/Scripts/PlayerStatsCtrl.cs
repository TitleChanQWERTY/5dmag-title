using TMPro;
using UnityEngine;

public sealed class PlayerStatsCtrl : MonoBehaviour
{
    [Header("Stats")]
    [SerializeField] private int coins;

    [Header("Refs")]
    [SerializeField] private TextMeshProUGUI _coinAmountText;

    public int Coins => coins;

    private void Start()
    {
        UpdateCoinsText();
    }

    public void AddCoins(int amount)
    {
        coins += amount;
        UpdateCoinsText();
    }

    private void UpdateCoinsText()
    {
        _coinAmountText.SetText("Player's coins: " + coins.ToString());
    }
}
