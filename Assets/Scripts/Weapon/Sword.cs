using UnityEngine;

public sealed class Sword : MonoBehaviour
{
    [SerializeField] private float weaponDamage;
    [SerializeField] private bool isPlayerWeapon;

    public float WeaponDamage => weaponDamage;
    public bool IsPlayerWeapon => isPlayerWeapon;
}
